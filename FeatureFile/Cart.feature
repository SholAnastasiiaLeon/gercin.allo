﻿Feature: Cart

 As a user
 I want to access to my cart
 In order to view a content of my order any time

 As a buyer
 I want to delete goods from my cart
 To have possibility to change my choice
 
 Background: 
 Given Allo website is open

Scenario: Unauthorized user has empty cart
 Given User is not logged in 
 When User click on cart 
 Then Cart is empty

 Scenario:  Unauthorization user can add goods to cart
 Given User is not logged in
 When User click to buy in product
 Then Product is in cart

 Scenario: Unauthorization user can delete goods from cart 
 Given In order there is a product
 When User click to x near the choose product
 Then Product is absent in cart