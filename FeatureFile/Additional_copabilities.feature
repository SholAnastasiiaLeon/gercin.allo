﻿Feature: Additional capabilities
		
		As a user who likes to watch reviews 
		I want to have a blog 
		in which I will watch the review, an interesting product for myself		 
		 
		 As an HR employee
		 I want to have page with vacancy
		 So that I can find workers easier and faster
		 
		 As a person, who get a Fishka card
		I want to have a page with info about Fishka card
		To know where else I can accumulate points

		 As a user
		 I want to have map with storage location
		 To better searching storage
		
		 As a new client
		 I want to have a page with info about delivery
		 To choose better for me
		 
		 As a client
		 I want to have full info about credit percentage from different banks
		 So that I can choose the best conditions for me
		 
		 As a client
		 I want to know that guaranty I can get
		 So I can read info about certain product
				
		As an owner
		I want to have a page with feedback from clients
		To know quality of work in my chain of stores
		 
		 As a user
		I want to have possibility to change language 
		To read info in website in my own language
		
		As a young family 
		We want to subscribe on newsletter
		So that we can get news about discount for products

		Background:
		Given Allo website is open

Scenario: Checking page review about products
	When User clicks on blog button
	Then User sees open blog page

Scenario: Checking vacancy page
	When User clicks on vacancy button
	Then User sees open vacancy page

Scenario: Checking fishka page
	When User clicks on fishka button
	Then User sees open fishka page

Scenario: Checking storages in main page 
	When User clicks storages button
	Then User sees open storages page

	Scenario: Checking delivery and pay in main page
	When User clicks delivery and pay logo 
	Then User sees open delivery and pay page

Scenario: Checking credit in main page
	When User clicks credit logo 
	Then User sees open credit page

Scenario: Checking guaranty and returning in main page
	When User clicks guaranty and returning logo
	Then User sees open guaranty and returning page
	
	Scenario: Checking contacts in main page
	When User clicks contacts button
	Then User sees open contacts page	

Scenario: Change language site to ukrainian 
	When User click to UA
	Then Site became in ukrainian language

Scenario: Subscription for news letters
	When Find field subscription for news
	When Enter fghjkjhgf@gmail.com to field email
	Then You have massage "Спасибо за подписку! Письмо с ссылкой для подтверждения подписки отослано на ваш адрес."