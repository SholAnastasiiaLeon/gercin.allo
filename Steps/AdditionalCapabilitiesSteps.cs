﻿using Gherkin.POM;
using System;
using TechTalk.SpecFlow;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium;
using NUnit.Framework;
/*
namespace Gherkin.Steps
{
    [Binding]
    public class AdditionalCapabilitiesSteps
    {
        public HeaderOfSitePOM headerOfSitePOM;
        public BlogPOM blogPOM;
        public IWebDriver driver;
        public Actions actions;
        public VacancyPOM vacancyPOM;
        public FishkaPOM fishkaPOM;
        public StoregesPOM storegesPOM;
        public DeliveryAndPaymentPOM deliveryAndPaymentPOM;
        public CreditPOM creditPOM;
        public QuarantyAndReturnPOM quarantyAndReturnPOM;
        public ContactsPOM contactsPOM;

        [Given(@"Allo website is open")]
        public void GivenAlloWebsiteIsOpen()
        {
            driver = new ChromeDriver(@"D:\DevEducatoin\CromeDriver");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(40);
            driver.Navigate().GoToUrl("https://allo.ua/ru/");
            driver.Manage().Window.Maximize();
            headerOfSitePOM = new HeaderOfSitePOM(driver);
            blogPOM = new BlogPOM(driver);
            vacancyPOM = new VacancyPOM(driver);
            fishkaPOM = new FishkaPOM(driver);
            storegesPOM = new StoregesPOM(driver);
            deliveryAndPaymentPOM = new DeliveryAndPaymentPOM(driver);
            creditPOM = new CreditPOM(driver);
            quarantyAndReturnPOM = new QuarantyAndReturnPOM(driver);
            contactsPOM = new ContactsPOM(driver);
        }
        //1
        [When(@"User clicks on blog button")]
        public void WhenUserClicksOnBlogButton()
        {
            headerOfSitePOM.EnterToBlogPage();
        }
        [Then(@"User sees open blog page")]
        public void ThenUserSeesOpenBlogPage()
        {
            string blogText = blogPOM.GetTextFromBlogLabel();
            Assert.AreEqual("Блог", blogText);
        }
        //2
        [When(@"User clicks on vacancy button")]
        public void WhenUserClicksOnVacancyButton()
        {
            headerOfSitePOM.EnterToVacancyPage();
        }
        [Then(@"User sees open vacancy page")]
        public void ThenUserSeesOpenVacancyPage()
        {
            string vacancyText = vacancyPOM.GetTextFromVacancyLabel();
            Assert.AreEqual("Тиць, і ти вже з нами", vacancyText);
        }
        //3
        [When(@"User clicks on fishka button")]
        public void WhenUserClicksOnFishkaButton()
        {
            headerOfSitePOM.EnterToFiskaPage();
        }
        [Then(@"User sees open fishka page")]
        public void ThenUserSeesOpenFishkaPage()
        {
            string vacancyText = fishkaPOM.GetTextFromFishkaLabel();
            Assert.AreEqual("Что такое Fishka?", vacancyText);
        }
        //4
        [When(@"User clicks storages button")]
        public void WhenUserClicksStoragesButton()
        {
            headerOfSitePOM.EnterToStoregesPage();
        }
        [Then(@"User sees open storages page")]
        public void ThenUserSeesOpenStoragesPage()
        {
            string storegesText = storegesPOM.GetTextFromStoragesLabel();
            Assert.AreEqual("Адреса магазинов", storegesText);
        }
        //5
        [When(@"User clicks delivery and pay logo")]
        public void WhenUserClicksDeliveryAndPayLogo()
        {
            headerOfSitePOM.EnterToDeliveryAndPayPage();
        }
        [Then(@"User sees open delivery and pay page")]
        public void ThenUserSeesOpenDeliveryAndPayPage()
        {
            string deliveryText = deliveryAndPaymentPOM.GetTextFromDeliveryLabel();
            Assert.AreEqual("Доставка и оплата", deliveryText);
        }
        //6
        [When(@"User clicks credit logo")]
        public void WhenUserClicksCreditLogo()
        {
            headerOfSitePOM.EnterToCreditPage();
        }
        [Then(@"User sees open credit page")]
        public void ThenUserSeesOpenCreditPage()
        {
            string creditText = creditPOM.GetTextCreditLabel();
            Assert.AreEqual("Кредит", creditText);
        }
        //7
        [When(@"User clicks guaranty and returning logo")]
        public void WhenUserClicksGuarantyAndReturningLogo()
        {
            headerOfSitePOM.EnterToGuarantyPage();
        }
        [Then(@"User sees open guaranty and returning page")]
        public void ThenUserSeesOpenGuarantyAndReturningPage()
        {
            string guarantyText = quarantyAndReturnPOM.GetTextgGuarantyLabel();
            Assert.AreEqual("ГАРАНТИЙНОЕ ОБСЛУЖИВАНИЕ", guarantyText);
        }
        //8
        [When(@"User clicks contacts button")]
        public void WhenUserClicksContactsButton()
        {
            headerOfSitePOM.EnterToContactsPage();
        }
        [Then(@"User sees open contacts page")]
        public void ThenUserSeesOpenContactsPage()
        {
            string contactsText = contactsPOM.GetTextContactsLabel();
            Assert.AreEqual("Консультации и заказы по телефонам", contactsText);
        }
        //9
        [When(@"User click to UA")]
        public void WhenUserClickToUA()
        {
            ScenarioContext.Current.Pending();
        }
        [Then(@"Site became in ukrainian language")]
        public void ThenSiteBecameInUkrainianLanguage()
        {
            ScenarioContext.Current.Pending();
        }
        //10
        [When(@"Find field subscription for news")]
        public void WhenFindFieldSubscriptionForNews()
        {
            ScenarioContext.Current.Pending();
        }       
        [When(@"Enter fghjkjhgf@gmail\.com to field email")]
        public void WhenEnterFghjkjhgfGmail_ComToFieldEmail()
        {
            ScenarioContext.Current.Pending();
        }      
        [Then(@"You have massage ""(.*)""")]
        public void ThenYouHaveMassage(string p0)
        {
            ScenarioContext.Current.Pending();
        }
    }
}*/
