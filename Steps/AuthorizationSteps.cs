﻿using Gherkin.POM;
using System;
using TechTalk.SpecFlow;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium;
using NUnit.Framework;
using OpenQA.Selenium.Support.UI;

namespace Gherkin.Steps
{
    [Binding]
    public class AuthorizationSteps
    {
        public IWebDriver driver;
        public HeaderOfSitePOM headerOfSitePOM;
        public AuthorizationPOM authorizationPOM;

        [Given(@"Allo website is open")]
        public void GivenAlloWebsiteIsOpen()
        {
            driver = new ChromeDriver(@"D:\DevEducatoin\CromeDriver");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(40);
            driver.Navigate().GoToUrl("https://allo.ua/ru/");
            driver.Manage().Window.Maximize();
            headerOfSitePOM = new HeaderOfSitePOM(driver);
            authorizationPOM = new AuthorizationPOM(driver);
        }

            //1
            [Given(@"User is logged out")]
        public void GivenUserIsLoggedOut()
        {
            string loggedOutUser = headerOfSitePOM.GetTextFromEntryButton();
            Assert.AreEqual("Вход Регистрация", loggedOutUser);
        }
            [When(@"Click to enter registration")]
        public void WhenClickToEnterRegistration()
        {
            headerOfSitePOM.EnterToAuthorizationPage();

        }
            [When(@"Enter ""(.*)"" in field e-mail")]
        public void WhenEnterInFieldE_Mail(string p0)
        {
            authorizationPOM.ChooseAuthorization();
            authorizationPOM.EnterTextToEmail("baneyev818@nnacell.com");
        }
            [When(@"Enter password ""(.*)""")]
        public void WhenEnterPassword(string p0)
        {
            authorizationPOM.EnterTextToPassword("111111");
            authorizationPOM.ConfirmEnterToAuthorizationPage();
        }
         
        [Then(@"User seens in entry field Anna")]
        public void ThenUserSeensInEntryFieldAnna()
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.TextToBePresentInElementLocated(authorizationPOM.authorizationUser, "Anna"));

        }

        //2
        [Given(@"User logged in")]
        public void GivenUserLoggedIn()
        {
            ScenarioContext.Current.Pending();
        }       
            [When(@"I add products to favorites")]
        public void WhenIAddProductsToFavorites()
        {
            ScenarioContext.Current.Pending();
        }      
            [Then(@"I click on user block")]
        public void ThenIClickOnUserBlock()
        {
            ScenarioContext.Current.Pending();
        }       
            [Then(@"I can enter button favorites products and view products in favorites products")]
        public void ThenICanEnterButtonFavoritesProductsAndViewProductsInFavoritesProducts()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
