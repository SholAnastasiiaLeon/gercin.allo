﻿using Gherkin.POM;
using System;
using TechTalk.SpecFlow;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium;
using NUnit.Framework;

namespace Gherkin.Steps
{
    [Binding]
    public class CartSteps
    {
        public HeaderOfSitePOM headerOfSitePOM;
        public CartPOM cartPOM;
        public IWebDriver driver;
        public Actions actions;
        public BlogPOM blogPOM;

    //    [Given(@"Allo website is open")]
    //    public void GivenAlloWebsiteIsOpen()
    //    {
    //        driver = new ChromeDriver(@"D:\DevEducatoin\CromeDriver");
    //        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(40);
    //        driver.Navigate().GoToUrl("https://allo.ua/ru/");
    //        driver.Manage().Window.Maximize();
    //        headerOfSitePOM = new HeaderOfSitePOM (driver);
    //        cartPOM = new CartPOM(driver);
    //        blogPOM = new BlogPOM(driver);
            
    //}
        
        [Given(@"User is not logged in")]
        public void GivenUserIsNotLoggedIn()
        {
            string notLogged = headerOfSitePOM.GetTextFromEntryButton();
            Assert.AreEqual("Вход Регистрация", notLogged);
        }

        [When(@"User click on cart")]
        public void WhenUserClickOnCart()
        {
            driver.FindElement(headerOfSitePOM.EntryToCartFromMP).Click();
        }

        [Then(@"Cart is empty")]
        public void ThenCartIsEmpty()
        {
            string emptyCart = cartPOM.GetTextFromCurtButton();
            Assert.AreEqual("сюда", emptyCart);
        }

        [When(@"User click to buy in product")]
        public void WhenUserClickToBuyInProduct()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"Product is in cart")]
        public void ThenProductIsInCart()
        {
            ScenarioContext.Current.Pending();
        }


        [Given(@"In order there is a product")]
        public void GivenInOrderThereIsAProduct()
        {
            ScenarioContext.Current.Pending();
        }      
        
        [When(@"User click to x near the choose product")]
        public void WhenUserClickToXNearTheChooseProduct()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"Product is absent in cart")]
        public void ThenProductIsAbsentInCart()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
