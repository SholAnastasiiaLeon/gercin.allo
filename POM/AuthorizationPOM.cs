﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;

namespace Gherkin.POM
{
    public class AuthorizationPOM
    {
        private IWebDriver _driver;

        public By authorizationTab = By.XPath("/html/body/div[3]/div/div/div[2]/div/ul/li[1]");
            public By fieldToEnterNumberOrMail = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[1]/form/div/div[1]/div[1]/div[1]/input");
            public By fieldToEnterPassword = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[1]/form/div/div[1]/div[2]/div[1]/input");
            public By loginButton = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[1]/form/div/button");
            public By authorizationUser = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[5]/div");
        public By entry = By.XPath("/html/body/div[3]/div/div/div[2]/div/ul/li[1]");

        public AuthorizationPOM(IWebDriver driver)
        {
            this._driver = driver;
        }
        public AuthorizationPOM EnterTextToEmail(string email)
        {
            _driver.FindElement(fieldToEnterNumberOrMail).SendKeys(email);
            return this;
        }
        public AuthorizationPOM EnterTextToPassword(string pass)
        {
            _driver.FindElement(fieldToEnterPassword).SendKeys(pass);
            return this;
        }
        public AuthorizationPOM ConfirmEnterToAuthorizationPage()
        {
            _driver.FindElement(loginButton).Click();
            return this;
        }
        public IWebElement FindAuthorizationLabel()
        {
            return _driver.FindElement(authorizationUser);
        }

        public string GetTextFromAuthorizationButton()
        {
            return FindAuthorizationLabel().Text;
        }
        public AuthorizationPOM ChooseAuthorization()
        {
            _driver.FindElement(entry).Click();
            return new AuthorizationPOM(_driver);
        }
    }
}
