﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;

namespace Gherkin.POM
{
    public class QuarantyAndReturnPOM
    {
        private IWebDriver _driver;

        public By quarantyAndReturnLabel = By.XPath("/html/body/div[4]/div[2]/div[2]/div/div/div[2]/div/div/section/div[1]/h2");

        public QuarantyAndReturnPOM(IWebDriver driver)
        {
            this._driver = driver;
        }
        public IWebElement FindQuarantyButton()
        {
            return _driver.FindElement(quarantyAndReturnLabel);
        }

        public string GetTextgGuarantyLabel()
        {
            return FindQuarantyButton().Text;
        }
    }

}
