﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;

namespace Gherkin.POM
{
    public class ContactsPOM
    {
        private IWebDriver _driver;
        private Actions actions;


        public By contactsLabel = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[1]/h3");

        public ContactsPOM(IWebDriver driver)
        {
            this._driver = driver;
            //actions = new Actions(driver);
        }

        public IWebElement FindContactsButton()
        {
            return _driver.FindElement(contactsLabel);
        }

        public string GetTextContactsLabel()
        {
            return FindContactsButton().Text;
        }
    }
}

