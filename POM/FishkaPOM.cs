﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;

namespace Gherkin.POM
{
    public class FishkaPOM
    {
        private IWebDriver _driver;

        public By fishkaLabel = By.XPath("/html/body/div[3]/div[2]/div[2]/div/div/div[2]/div/div[2]/div[1]/div[1]/h2");

        public FishkaPOM(IWebDriver driver)
        {
            this._driver = driver;
        }

        public IWebElement FindFiskaButton()
        {
            return _driver.FindElement(fishkaLabel);
        }

        public string GetTextFromFishkaLabel()
        {
            return FindFiskaButton().Text;
        }

    }
}
