﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;

namespace Gherkin.POM
{
    public class BlogPOM
    {
        private IWebDriver _driver;

        public By blogLabel = By.XPath("/html/body/div[2]/header/div/div/div/div/div[1]/div/a/img");

        public BlogPOM(IWebDriver driver)
        {
            this._driver = driver;
        }
        public IWebElement FindBlogButton()
        {
            return _driver.FindElement(blogLabel);
        }

        public string GetTextFromBlogLabel()
        {
            return FindBlogButton().Text;
        }
    }

}
