﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;

namespace Gherkin.POM
{
    public class StoregesPOM
    {
        private IWebDriver _driver;

        public By adressOfStoreges = By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/div[1]/h1");
        public By dniproinsert = By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/ul/li[1]/a");
        public By mapDnipro = By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/div[2]/div[1]/img");
        public By firstAdressInDniproInsert = By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/div[3]/div/div/table/tbody/tr[1]/td[2]");

        public StoregesPOM(IWebDriver driver)
        {
            this._driver = driver;
        }
        public IWebElement FindStoragesButton()
        {
            return _driver.FindElement(adressOfStoreges);
        }

        public string GetTextFromStoragesLabel()
        {
            return FindStoragesButton().Text;
        }
    }
}