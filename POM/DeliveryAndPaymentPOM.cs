﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
namespace Gherkin.POM
{
    public class DeliveryAndPaymentPOM
    {
        private IWebDriver _driver;

        public By deliveryAndPaymentLabel = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/h2");        public DeliveryAndPaymentPOM(IWebDriver driver)
        {
            this._driver = driver;
        }        public IWebElement FindDeliveryButton()
        {
            return _driver.FindElement(deliveryAndPaymentLabel);
        }

        public string GetTextFromDeliveryLabel()
        {
            return FindDeliveryButton().Text;
        }
    }
}