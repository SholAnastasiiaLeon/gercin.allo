﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;

namespace Gherkin.POM
    
{
    public class HeaderOfSitePOM
    {
        private IWebDriver _driver;
        private Actions actions;


        public By mainLabelAllo = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/a/img[1]");
        public By iconLocation = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[1]/div/svg[1]");
        public By nameOfLocation = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[1]/div");
        public By darkTheme = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[2]/span[1]");
        public By toggleToChangeTheme = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[2]/div");
        public By lightTheme = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[2]/span[2]");
        public By blog = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[1]/a");       
        public By fishka = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[2]/a");
        public By vacancy = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[3]/a");
        public By storeges = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[4]/a");
        public By deliveryAndPay = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[5]/a");
        public By credit = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[6]/a");
        public By quarantyVozwrat = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[7]/a");
        public By contacts = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[8]/a");
        public By languageRU = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[4]/span[1]");
        public By languageUA = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[4]/span[3]");
        public By toggleToChangeLanguage = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[4]/span[2]/div");
        public By liveSmart = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[1]/a/p");
        public By alloMoney = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[2]/a/p");
        public By alloUpgrade = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[3]/a/p");
        public By alloExchenge = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[4]/a/p");
        public By ucenka = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[5]/a/p");
        public By akcyi = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[6]/a/p");
        public By EntryToCartFromMP= By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[6]/div/div[1]");
        public By entryToAccount = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[5]/div/div");

        public HeaderOfSitePOM(IWebDriver driver)
        {
            this._driver = driver;
            actions = new Actions(driver);
        }
        public IWebElement FindEntryButton()
        {
            return _driver.FindElement(entryToAccount);
        }

        public string GetTextFromEntryButton()
        {
            return FindEntryButton().Text;
        }

        public BlogPOM EnterToBlogPage()
        {
            _driver.FindElement(blog).Click();
            return new BlogPOM(_driver);
        }
        public VacancyPOM EnterToVacancyPage()
        {
            _driver.FindElement(vacancy).Click();
            return new VacancyPOM(_driver);
        }
        public FishkaPOM EnterToFiskaPage()
        {
            _driver.FindElement(fishka).Click();
            return new FishkaPOM(_driver);
        }
        public StoregesPOM EnterToStoregesPage()
        {
            _driver.FindElement(storeges).Click();
            return new StoregesPOM(_driver);
        }
        public DeliveryAndPaymentPOM EnterToDeliveryAndPayPage()
        {
            _driver.FindElement(deliveryAndPay).Click();
            return new DeliveryAndPaymentPOM(_driver);
        }
        public CreditPOM EnterToCreditPage()
        {
            _driver.FindElement(credit).Click();
            return new CreditPOM(_driver);
        }
        public QuarantyAndReturnPOM EnterToGuarantyPage()
        {
            _driver.FindElement(quarantyVozwrat).Click();
            return new QuarantyAndReturnPOM(_driver);
        }
        public ContactsPOM EnterToContactsPage()
        {
            _driver.FindElement(contacts).Click();
            return new ContactsPOM(_driver);
        }
        public AuthorizationPOM EnterToAuthorizationPage()
        {
            _driver.FindElement(entryToAccount).Click();
            return new AuthorizationPOM(_driver);
        }



    }
}