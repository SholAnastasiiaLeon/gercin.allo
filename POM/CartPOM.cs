﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;

namespace Gherkin.POM
{
    public class CartPOM
    {
        private IWebDriver _driver;
        private Actions actions;

        
        public By yourCartIsEmpty= By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/p[2]/a");
        public By returnToBuying = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/p[2]");
        public By link = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/p[2]/a");
        public By closeCart = By.XPath("/html/body/div[3]/div/div/div[1]/svg/use//svg/path");


        public CartPOM(IWebDriver driver)
        {
            this._driver = driver;
            actions = new Actions(driver);
        }
        public IWebElement FindCurtButton()
        {
            return _driver.FindElement(yourCartIsEmpty);
        }

        public string GetTextFromCurtButton()
        {
            return FindCurtButton().Text;
        }
    }
}
