﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;

namespace Gherkin.POM
{
    public class VacancyPOM
    {
        private IWebDriver _driver;

        public By startCareerLabelITyZNamy = By.XPath("/html/body/div[2]/div[3]/div/div/div/div/article/div/div[1]/div/div/div[1]/div/h2");


        public VacancyPOM(IWebDriver driver)
        {
            this._driver = driver;
        }
        public IWebElement FindVacancyButton()
        {
            return _driver.FindElement(startCareerLabelITyZNamy);
        }

        public string GetTextFromVacancyLabel()
        {
            return FindVacancyButton().Text;
        }
    }
}