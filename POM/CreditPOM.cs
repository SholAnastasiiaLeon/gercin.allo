﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;

namespace Gherkin.POM
{
    public class CreditPOM
    {
        private IWebDriver _driver;

        public By creditLabel = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/h2");

        public CreditPOM(IWebDriver driver)
        {
            this._driver = driver;
        }
        public IWebElement FindCreditButton()
        {
            return _driver.FindElement(creditLabel);
        }

        public string GetTextCreditLabel()
        {
            return FindCreditButton().Text;
        }
    }
}
